const bodyParser = require('body-parser')
const express = require('express')
const app = express()
var mongo = require('mongodb');

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://172.17.0.2:27017/mydb";


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/',  function (req, res) {
  res.send('Hello World')
})

app.listen(8080, () => {
  console.log('Start server at port 8080.')
})


MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("db_name");
  dbo.createCollection("employee", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

app.post('/employee',  function (req, res) {
	  MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		    if (err) res.send(err);
		    var dbo = db.db("db_name");
		    dbo.collection("employee").insertOne(req.body, function(err, res2) {
			        if (err) throw err;
			        console.log("Inserted");
			    	res.send(req.body)
			        db.close();
			      });
	  });
	  
})

app.get('/employee', function (req, res) {
		MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
			  if (err) throw err;
			  var dbo = db.db("db_name");
			  dbo.collection("employee").find({}).toArray(function(err, result) {
				      if (err) throw err;
				      console.log(result);
				  	res.send(result);
				      db.close();
				    });
		});
})

app.get('/employee/:employee_id',  function (req, res) {
	 MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
		   if (err) throw err;
		   var dbo = db.db("db_name");
		   var query = { employee_id: req.params.employee_id };
		   dbo.collection("employee").find(query).toArray(function(err, result) {
			       if (err) throw err;
			       console.log(result);
			   	res.send(result)
			       db.close();
			     });
	 });
})

app.put('/employee/:employee_id',  function (req, res) {
	  MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
		    if (err) res.send(err);
		    var dbo = db.db("db_name");
		    var newvalues = { $set: req.body };
		    dbo.collection("employee").updateOne({ employee_id: req.params.employee_id }, newvalues, function(err2, res2) {
			        if (err2) res.send(err2);
			    	console.log("Updated employee_id: "+ req.params.employee_id);
			    	res.send("Updated employee_id: "+ req.params.employee_id);
			        db.close();
			      });
	  });
})

app.delete('/employee/:employee_id', function (req, res) {
	  MongoClient.connect(url, function(err, db) {
		    if (err) throw err;
		    var dbo = db.db("db_name");
		    var myquery = { employee_id: req.params.employee_id  };
		    dbo.collection("employee").deleteOne(myquery,{ useNewUrlParser: true }, function(err, obj) {
			        if (err) throw err;
			        console.log("Deleted employee_id: "+ req.params.employee_id);
			    	res.send("Deleted employee_id: "+ req.params.employee_id);
			        db.close();
			      });
	  });
	  
})


